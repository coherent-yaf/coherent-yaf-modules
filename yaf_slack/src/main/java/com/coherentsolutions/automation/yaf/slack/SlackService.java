package com.coherentsolutions.automation.yaf.slack;

/*-
 * #%L
 * Yaf Slack Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.consts.Consts;
import com.coherentsolutions.automation.yaf.core.events.global.ExecutionReportReadyEvent;
import com.coherentsolutions.automation.yaf.core.report.domain.ExecutionReport;
import com.github.seratch.jslack.Slack;
import com.github.seratch.jslack.api.webhook.Payload;
import com.github.seratch.jslack.api.webhook.WebhookResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
@ConditionalOnProperty(name = Consts.FRAMEWORK_NAME + ".slack.enabled", havingValue = "true")
public class SlackService {

    @Autowired
    SlackProperties properties;

    @Autowired(required = false)
    SlackExecutionReportMessagePayload reportMessagePayload;

    @EventListener
    public void publishReport(ExecutionReportReadyEvent event) {
        if (properties.isSlackReport()) {
            // send general report to slack
            ExecutionReport report = event.getReport();
            if (reportMessagePayload != null) {
                sendMessage(reportMessagePayload.getReportPayload(report));
            } else {
                log.error("Unable to send slack report cause message payload bean is null");
            }
        }
    }

    public WebhookResponse sendMessage(Payload payload) {
        try {
            return Slack.getInstance().send(properties.getWebhookUrl(), payload);
        } catch (IOException e) {
            // TODO
            log.error(e.getMessage(), e);
            return null;
        }
    }
}
