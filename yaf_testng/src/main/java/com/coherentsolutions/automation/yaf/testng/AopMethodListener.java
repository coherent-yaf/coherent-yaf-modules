package com.coherentsolutions.automation.yaf.testng;

/*-
 * #%L
 * Yaf TestNG Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.interceptor.InterceptorUtils;
import com.coherentsolutions.automation.yaf.core.interceptor.MethodCallInterceptor;
import com.coherentsolutions.automation.yaf.core.test.BaseYafTest;
import com.coherentsolutions.automation.yaf.core.utils.YafReflectionUtils;
import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;
import javassist.util.proxy.ProxyObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ReflectionUtils;
import org.testng.IMethodInstance;
import org.testng.IMethodInterceptor;
import org.testng.ITestContext;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Deprecated // check?
public class AopMethodListener implements IMethodInterceptor {

    private List<MethodCallInterceptor> mci = null;

    protected List<MethodCallInterceptor> getMCIList(Object obj) {
        if (mci != null) {
            return mci;
        }
        if (obj instanceof BaseYafTest) {
            BaseYafTest tst = (BaseYafTest) obj;
            mci = tst.getApplicationContext().getBeansOfType(MethodCallInterceptor.class).values().stream()
                    .collect(Collectors.toList());
            return mci;
        } else {
            log.error("Could not get MCI cause instance is not a BaseYafTest and application context is unreachable!");
            return null;
        }
    }

    @Override
    public List<IMethodInstance> intercept(List<IMethodInstance> methods, ITestContext context) {

        methods.stream().forEach(m -> {

            Object instance = m.getInstance();

            List<MethodCallInterceptor> mciList = getMCIList(instance);
            if (mciList != null && !mciList.isEmpty()) {
                ProxyFactory factory = new ProxyFactory();
                factory.setSuperclass(instance.getClass());

                MethodHandler handler = (self, overridden, forwarder, args) -> InterceptorUtils.interceptMethod(mciList,
                        instance, overridden, args);

                factory.setFilter(method -> {
                    boolean res = mciList.stream().anyMatch(mci -> mci.isApplicableToMethod(method));
                    return res;
                });

                try {
                    Object proxyInstance = factory.createClass().newInstance();
                    ((ProxyObject) proxyInstance).setHandler(handler);
                    System.out.println("Create proxy instance " + proxyInstance);

                    Field m_instance = ReflectionUtils.findField(m.getMethod().getClass(), "m_instance");
                    YafReflectionUtils.setFieldValue(m_instance, proxyInstance, m.getMethod());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return methods;
    }
}
