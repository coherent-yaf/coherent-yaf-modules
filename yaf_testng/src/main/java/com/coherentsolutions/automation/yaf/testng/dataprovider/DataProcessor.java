package com.coherentsolutions.automation.yaf.testng.dataprovider;

/*-
 * #%L
 * Yaf TestNG Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.core.exception.DataYafException;
import com.coherentsolutions.automation.yaf.testng.dataprovider.model.Args;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;

public abstract class DataProcessor<T extends Args> {

    protected abstract Class getSupportedAnnotationClass();

    protected abstract List<T> process(Method method, Annotation annotation, TestExecutionContext testExecutionContext)
            throws DataYafException;

    public List<T> processData(Method method, Annotation annotation, TestExecutionContext testExecutionContext)
            throws DataYafException {
        if (annotation != null && annotation.annotationType().equals(getSupportedAnnotationClass())) {
            return process(method, annotation, testExecutionContext);
        } else {
            throw new DataYafException("Could not find proper processor class for annotation " + annotation);
        }

    }
}
