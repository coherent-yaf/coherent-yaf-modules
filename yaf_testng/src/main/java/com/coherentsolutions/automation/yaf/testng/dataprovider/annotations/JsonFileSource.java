package com.coherentsolutions.automation.yaf.testng.dataprovider.annotations;

/*-
 * #%L
 * Yaf TestNG Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.core.exception.DataYafException;
import com.coherentsolutions.automation.yaf.core.utils.FileUtils;
import com.coherentsolutions.automation.yaf.testng.dataprovider.DataProcessor;
import com.coherentsolutions.automation.yaf.testng.dataprovider.model.Args;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import static java.lang.annotation.ElementType.METHOD;

@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@Target({ METHOD })
@Source(processor = JsonFileSource.Processor.class)
public @interface JsonFileSource {

    String name();

    String folder() default "data";

    Class cls();

    @Component
    class Processor extends DataProcessor<Args> {

        @Autowired
        ObjectMapper mapper;

        @Override
        public Class getSupportedAnnotationClass() {
            return JsonFileSource.class;
        }

        @Override
        public List<Args> process(Method method, Annotation annotation, TestExecutionContext testExecutionContext)
                throws DataYafException {
            JsonFileSource source = (JsonFileSource) annotation;
            try {
                Object obj = mapper.readValue(FileUtils.getResourceJsonFile(source.name(), source.folder()),
                        source.cls());
                return Arrays.asList(Args.build(obj));
            } catch (Exception e) {
                throw new DataYafException("Unable to read file", e);
            }
        }
    }
}
