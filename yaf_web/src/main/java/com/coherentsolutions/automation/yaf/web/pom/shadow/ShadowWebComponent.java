package com.coherentsolutions.automation.yaf.web.pom.shadow;

/*-
 * #%L
 * Yaf Web Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.exception.DriverYafException;
import com.coherentsolutions.automation.yaf.core.utils.YafBeanUtils;
import com.coherentsolutions.automation.yaf.web.pom.WebComponent;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Data
public abstract class ShadowWebComponent extends WebComponent {
    protected List<String> shadowRoots = new ArrayList<>();

    @Autowired
    YafBeanUtils beanUtils;

    @Override
    protected void init(Field field, Object obj, Class objType, List<Annotation> annotations, String driverName) {

        // TODO read from field annotations if any
        ShadowRoot annotation = beanUtils.getAnnotation(this.getClass(), ShadowRoot.class);
        if (annotation != null) {
            if (beanUtils.isChild(objType, ShadowWebPage.class)) {
                // we have parent shadow page
                ShadowWebPage root = (ShadowWebPage) obj;
                shadowRoots.addAll(root.getShadowRoots());
            }
            shadowRoots.add(annotation.value());
            super.init(field, obj, objType, annotations, driverName);
        } else {
            throw new DriverYafException("Could not init shadow page without root selector specification!");
        }
    }
}
