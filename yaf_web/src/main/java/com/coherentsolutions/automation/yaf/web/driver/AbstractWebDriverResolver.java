package com.coherentsolutions.automation.yaf.web.driver;

/*-
 * #%L
 * Yaf Web Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.domain.device.BrowserDevice;
import com.coherentsolutions.automation.yaf.core.config.env.domain.device.Device;
import com.coherentsolutions.automation.yaf.core.drivers.manager.DriverResolver;
import com.coherentsolutions.automation.yaf.core.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.core.enums.DeviceType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class AbstractWebDriverResolver extends DriverResolver {

    private static final String HEADLESS = "-headless";

    @Autowired
    protected WebDriverProperties properties;

    @Override
    public DeviceType getResolverType() {
        return DeviceType.WEB;
    }

    protected ChromeDriver buildChromeDriver(BrowserDevice device) {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.merge(buildCapabilitiesFromEnv(device));
        chromeOptions.setHeadless(device.isHeadless());
        chromeOptions.addArguments("--disable-popup-blocking");
        chromeOptions.addArguments("--disable-notifications");
        return new ChromeDriver(chromeOptions);
    }

    protected FirefoxDriver buildFireFoxDriver(BrowserDevice device) {
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.merge(buildCapabilitiesFromEnv(device));
        firefoxOptions.setHeadless(device.isHeadless());
        return new FirefoxDriver(firefoxOptions);
    }

    protected SafariDriver buildSafariDriver(BrowserDevice device) {
        SafariOptions safariOptions = new SafariOptions();
        safariOptions.merge(buildCapabilitiesFromEnv(device));
        return new SafariDriver(safariOptions);
    }

    protected EdgeDriver buildEdgeDriver(BrowserDevice device) {
        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.merge(buildCapabilitiesFromEnv(device));
        return new EdgeDriver(edgeOptions);
    }

    protected InternetExplorerDriver buildIEDriver(BrowserDevice device) {
        InternetExplorerOptions ieOptions = new InternetExplorerOptions();
        ieOptions.merge(buildCapabilitiesFromEnv(device));
        return new InternetExplorerDriver(ieOptions);
    }

    // TODO: investigate whet use instead of OperaDriver
    /*
     * protected OperaDriver buildOperaDriver(BrowserDevice device) { OperaOptions operaOptions = new OperaOptions();
     * operaOptions.merge(buildCapabilitiesFromEnv(device)); if (device.isHeadless()) {
     * operaOptions.addArguments(HEADLESS); } return new OperaDriver(operaOptions); }
     */

    @Override
    public DriverHolder initDriver(Device device) {
        DriverHolder holder = getDriverHolder(device);
        if (properties.isMaximizeBrowser()) {
            holder.getDriver().manage().window().maximize();
        }
        return holder;
    }

    protected DriverHolder buildHolder(Device device, WebDriver webDriver) {
        return super.buildHolder(device, webDriver, properties);
    }

    protected abstract DriverHolder getDriverHolder(Device device);
}
