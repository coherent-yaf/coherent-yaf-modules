package com.coherentsolutions.automation.yaf.web.driver;

/*-
 * #%L
 * Yaf Web Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.domain.device.Device;
import com.coherentsolutions.automation.yaf.core.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.web.driver.holder.ContainerDriverHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.testcontainers.containers.BrowserWebDriverContainer;

@Component
@Slf4j
public class ContainerWebDriverResolver extends AbstractWebDriverResolver {

    @Override
    public boolean canResolve(Device device) {
        return super.canResolve(device) && farmName(device, "container");
    }

    @Override
    protected DriverHolder getDriverHolder(Device device) {
        BrowserWebDriverContainer<?> c = new BrowserWebDriverContainer<>()
                .withCapabilities(buildCapabilitiesFromEnv(device));

        c.start();
        log.info("Selenium URL {}", c.getSeleniumAddress());
        log.info("VNC URL {}", c.getVncAddress());
        ContainerDriverHolder driverHolder = new ContainerDriverHolder(device, properties);
        driverHolder.setDriver(c.getWebDriver());
        driverHolder.setContainer(c);
        return driverHolder;
    }
}
