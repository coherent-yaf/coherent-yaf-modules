package com.coherentsolutions.automation.yaf.web.utils;

/*-
 * #%L
 * Yaf Web Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.JavascriptExecutor;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class BrowserLocalStorage extends WebUtils {

    public static final String LS = "window.localStorage";

    protected Object executeJavaScript(String script, Object... args) {
        return ((JavascriptExecutor) getWebDriver()).executeScript(script, args);
    }

    public void removeItem(String itemName) {
        executeJavaScript(LS + ".removeItem('" + itemName + "');");
    }

    public boolean isItemPresent(String itemName) {
        return getItemValue(itemName) != null;
    }

    public String getItemValue(String itemName) {
        return (String) executeJavaScript("return " + LS + ".getItem('" + itemName + "');");
    }

    public Long getLength() {
        return (Long) executeJavaScript("return " + LS + ".length;");
    }

    public void setItem(String itemName, String value) {
        executeJavaScript(String.format(LS + ".setItem('%s','%s');", itemName, value));
    }

    public void clear() {
        executeJavaScript(LS + "clear();");
    }

    public Map<String, String> getAll() throws JsonProcessingException {
        String ls = (String) executeJavaScript("return JSON.stringify(" + LS + ");");
        return new ObjectMapper().readValue(ls, new TypeReference<Map<String, String>>() {
        });
    }
}
