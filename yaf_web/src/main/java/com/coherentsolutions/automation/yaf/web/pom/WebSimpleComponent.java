package com.coherentsolutions.automation.yaf.web.pom;

/*-
 * #%L
 * Yaf Web Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.utils.by.YafBy;
import com.coherentsolutions.automation.yaf.core.wait.driver.WaitConsts;
import org.openqa.selenium.WebElement;

public abstract class WebSimpleComponent extends WebComponent {

    protected WebElement cachedElement;

    public abstract YafBy getElementSelector();

    public abstract void setElementSelector(YafBy elementSelector);

    protected abstract WebElement getParent();

    protected abstract void setParent(WebElement parent);

    protected WebElement getElement() {
        if (cachedElement == null) {
            if (getParent() != null) {
                cachedElement = getParent().findElement(getElementSelector());
            } else {
                cachedElement = driverHolder.getDriver().findElement(getElementSelector());
            }
        }
        return cachedElement;
    }

    public void waitForAppear(WaitConsts... waitConsts) {
        waitService.visible(getElement(), waitConsts);
    }

    public void waitForDisappear(WaitConsts... waitConsts) {
        waitService.invisible(getElement(), waitConsts);
    }
}
