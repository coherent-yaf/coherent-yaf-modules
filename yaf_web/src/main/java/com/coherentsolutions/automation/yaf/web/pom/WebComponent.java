package com.coherentsolutions.automation.yaf.web.pom;

/*-
 * #%L
 * Yaf Web Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.pom.Page;
import com.coherentsolutions.automation.yaf.core.pom.factory.YafLocatorFactory;
import com.coherentsolutions.automation.yaf.web.driver.WebDriverProperties;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;

public abstract class WebComponent extends Page {

    @Autowired
    WebDriverProperties properties;

    @Autowired
    YafLocatorFactory locatorFactory;

    @Override
    protected void init(Field field, Object obj, Class objType, List<Annotation> annotations, String driverName) {

        driverHolder = testExecutionContext.getWebDriverHolder(driverName);

        if (properties.isUsePageFactory()) {
            locatorFactory.setSearchContext(driverHolder.getDriver());
            locatorFactory.setPage(this);
            PageFactory.initElements(locatorFactory, this);
        }
    }

}
