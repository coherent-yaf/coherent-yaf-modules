package com.coherentsolutions.automation.yaf.allure;

/*-
 * #%L
 * Yaf Allure Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.consts.Consts;
import com.coherentsolutions.automation.yaf.core.enums.Severity;
import com.coherentsolutions.automation.yaf.core.events.test.TestFinishEvent;
import com.coherentsolutions.automation.yaf.core.events.test.TestStartEvent;
import com.coherentsolutions.automation.yaf.core.test.YafTest;
import com.coherentsolutions.automation.yaf.core.test.model.TestInfo;
import io.qameta.allure.Allure;
import io.qameta.allure.AllureLifecycle;
import io.qameta.allure.listener.LifecycleNotifier;
import io.qameta.allure.listener.TestLifecycleListener;
import io.qameta.allure.model.Label;
import io.qameta.allure.model.Link;
import io.qameta.allure.model.TestResult;
import io.qameta.allure.util.ResultsUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
@Slf4j
@ConditionalOnProperty(name = Consts.FRAMEWORK_NAME + ".allure.enabled", havingValue = "true")
public class AllureService {

    AllureLifecycle lifecycle;
    @Autowired
    AllureProperties allureProperties;

    Map<String, YafTest> testStore = new ConcurrentHashMap<>();

    public AllureService() {
        lifecycle = Allure.getLifecycle();

        // TODO add description why we need such complex solution

        // patch allure lifecycle
        try {
            Field f = ReflectionUtils.findField(AllureLifecycle.class, "notifier");
            f.setAccessible(true);
            LifecycleNotifier notifier = (LifecycleNotifier) f.get(lifecycle);

            Field ff = ReflectionUtils.findField(LifecycleNotifier.class, "testListeners");
            ff.setAccessible(true);
            List<TestLifecycleListener> cn = (List<TestLifecycleListener>) ff.get(notifier);
            cn.add(new TestPatcher());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @EventListener
    // @Async
    public void testStart(TestStartEvent testStartEvent) {

        YafTest yafTest = testStartEvent.getTestInfo().getYafTest();
        if (yafTest != null) {
            TestInfo testInfo = testStartEvent.getTestInfo();
            testStore.put(testInfo.getTestClass().getName() + "." + testInfo.getTestMethodName(), yafTest);
        }

    }

    @EventListener
    // @Async // todo validate async>
    public void testEnd(TestFinishEvent testFinishEvent) {
        // append attachments
        if (allureProperties.isFullLogAllTests() || !testFinishEvent.getTestResult().isSuccess()) {
            lifecycle.getCurrentTestCaseOrStep().ifPresent(parentUuid -> {

                // process attachments

                testFinishEvent.getLogData().forEach(ld -> {
                    try {
                        lifecycle.addAttachment(ld.getLogDataName(), ld.getContentType(), ld.getFileExt(),
                                ld.getData());
                    } catch (Exception e) {
                        log.error("Unable to append log data attach cause " + e.getMessage(), e);
                    }
                });
            });
        }
    }

    public class TestPatcher implements TestLifecycleListener {

        @Override
        public void beforeTestWrite(TestResult result) {
            YafTest yafTest = testStore.remove(result.getFullName());
            if (yafTest != null) {
                result.setName(yafTest.name());
                result.setDescription(yafTest.description());

                List<Label> labels = result.getLabels();
                List<Link> links = result.getLinks();
                if (labels == null) {
                    labels = new ArrayList<>();
                }
                if (links == null) {
                    links = new ArrayList<>();
                }
                if (!yafTest.severity().equals(Severity.UNKNOWN)) {
                    Label severity = ResultsUtils.createSeverityLabel(yafTest.severity().name());
                    labels.add(severity);
                }
                if (yafTest.tmsIds().length > 0) {
                    links.addAll(Arrays.stream(yafTest.tmsIds()).map(l -> ResultsUtils.createTmsLink(l))
                            .collect(Collectors.toList()));
                }
                if (yafTest.bugs().length > 0) {
                    links.addAll(Arrays.stream(yafTest.bugs()).map(b -> ResultsUtils.createIssueLink(b))
                            .collect(Collectors.toList()));
                }
                labels.add(ResultsUtils.createThreadLabel());
                // TODO may add other options
                result.setLabels(labels);
                result.setLinks(links);

            }
        }
    }

}
