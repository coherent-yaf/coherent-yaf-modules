package com.coherentsolutions.automation.yaf.mobile.driver;

/*-
 * #%L
 * Yaf Mobile Module
 * %%
 * Copyright (C) 2020 - 2024 Coherent Solutions Inc.
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.domain.device.MobileDevice;
import com.coherentsolutions.automation.yaf.core.drivers.manager.DriverResolver;
import com.coherentsolutions.automation.yaf.core.enums.DeviceType;
import com.coherentsolutions.automation.yaf.mobile.MobileProperties;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.Map;

import static com.coherentsolutions.automation.yaf.core.enums.DeviceType.MOBILE;
import static io.appium.java_client.remote.AndroidMobileCapabilityType.AVD;
import static io.appium.java_client.remote.MobileCapabilityType.AUTOMATION_NAME;
import static io.appium.java_client.remote.MobileCapabilityType.UDID;

@Component
@Slf4j
public abstract class AbstractMobileDriverResolver extends DriverResolver {

    @Override
    public DeviceType getResolverType() {
        return MOBILE;
    }

    // TODO add custom caps and device uids and proprs after prepareEnv

    protected MutableCapabilities getGeneralMobileCapabilities(MobileDevice device,
                                                               MobileProperties.AppiumMobileProperties properties, Map<String, String> customCapabilities) {
        MutableCapabilities capabilities = (MutableCapabilities) buildCapabilitiesFromEnv(device);

        properties.getConstsCapabilities().entrySet()
                .forEach(e -> capabilities.setCapability(e.getKey(), e.getValue()));

        customCapabilities.entrySet().forEach(e -> capabilities.setCapability(e.getKey(), e.getValue()));

        String udid = device.getCapabilities().get(MobileCapabilityType.UDID);

        if (device.isSimulator()) {
            capabilities.setCapability(AVD, udid);
        } else {
            capabilities.setCapability(UDID, udid);
        }
        return capabilities;
    }

    protected AndroidDriver buildAndroidDriver(MobileDevice device, URL serviceUrl,
                                               MobileProperties.AppiumMobileProperties properties, Map<String, String> customCapabilities) {
        MutableCapabilities capabilities = getGeneralMobileCapabilities(device, properties, customCapabilities);
        capabilities.setCapability(AUTOMATION_NAME, "UiAutomator2");
        capabilities.setCapability("allowInvisibleElements", true);
        return new AndroidDriver<>(serviceUrl, new DesiredCapabilities(capabilities));
    }

    protected IOSDriver buildIOSDriver(MobileDevice device, URL serviceUrl,
                                       MobileProperties.AppiumMobileProperties properties, Map<String, String> customCapabilities) {
        MutableCapabilities capabilities = getGeneralMobileCapabilities(device, properties, customCapabilities);
        capabilities.setCapability(AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability("platformName", "iOS");
        return new IOSDriver<>(serviceUrl, capabilities);
    }

    protected AppiumDriverLocalService startAppiumService(MobileProperties.AppiumMobileProperties properties) {
        AppiumServiceBuilder builder = new AppiumServiceBuilder();

        MobileProperties.AppiumServiceProperties appiumServiceProperties = properties.getService();

        if (appiumServiceProperties.getIp() != null) {
            builder = builder.withIPAddress(appiumServiceProperties.getIp());
        }
        if (appiumServiceProperties.getPort() != 0) {
            builder.usingPort(appiumServiceProperties.getPort());
        }

        AppiumDriverLocalService service = builder.build();

        service.start();
        log.info("Appium Service started with URL {} ", service.getUrl());

        return service;
    }
}
