package com.coherentsolutions.automation.yaf.mobile.driver;

/*-
 * #%L
 * Yaf Mobile Module
 * %%
 * Copyright (C) 2020 - 2024 Coherent Solutions Inc.
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.domain.device.Device;
import com.coherentsolutions.automation.yaf.core.config.env.domain.device.MobileDevice;
import com.coherentsolutions.automation.yaf.core.drivers.model.DriverHolder;
import com.coherentsolutions.automation.yaf.core.exception.DriverYafException;
import com.coherentsolutions.automation.yaf.mobile.MobileProperties;
import com.coherentsolutions.automation.yaf.mobile.driver.holder.AppiumDriverHolder;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.coherentsolutions.automation.yaf.core.enums.MobileOS.ANDROID;

@Component
@Order() // set this resolver at last position, cause other user defined should be prior that
@Slf4j
public class BaseMobileDriverResolver extends AbstractMobileDriverResolver {

    @Autowired
    MobileProperties mobileProperties;

    @Override
    public boolean canResolve(Device device) {
        return super.canResolve(device) && emptyFarm(device);
    }

    @Override
    public DriverHolder initDriver(Device device) {
        MobileDevice mobileDevice = (MobileDevice) device;
        WebDriver driver = null;
        log.debug("Building mobile {} driver for env {}", mobileDevice.getMobileOs(), mobileDevice.getName());

        AppiumDriverHolder appiumDriverHolder = new AppiumDriverHolder(device);

        MobileProperties.AppiumMobileProperties props = ANDROID.equals(mobileDevice.getMobileOs())
                ? mobileProperties.getAndroid() : mobileProperties.getIos();
        AppiumDriverLocalService service = startAppiumService(props);

        Map<String, String> customCapabilities = new HashMap<>();

        try {

            switch (mobileDevice.getMobileOs()) {
                case ANDROID: {

                    driver = buildAndroidDriver(mobileDevice, service.getUrl(), props, customCapabilities);
                    break;
                }
                case IOS: {

                    driver = buildIOSDriver(mobileDevice, service.getUrl(), props, customCapabilities);
                    break;
                }
                // todo add others
                default: {
                    throw new DriverYafException("SSSSSS");
                }
            }

            log.debug("****** CREATE NEW MOBILE DRIVER ******** ");

            appiumDriverHolder.setDriver(driver);
            appiumDriverHolder.setAppiumService(service);
        } finally {
            if (appiumDriverHolder.getDriver() == null) {
                log.debug("Lets stop service, cause we could not create mobile driver!");
                service.stop();
            }
        }
        return appiumDriverHolder;
    }

}
