package com.coherentsolutions.automation.yaf.mobile.utils.network;

/*-
 * #%L
 * Yaf Mobile Module
 * %%
 * Copyright (C) 2020 - 2024 Coherent Solutions Inc.
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.mobile.condition.Android;
import io.appium.java_client.android.AndroidDriver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Android
public class AndroidNetworkUtils extends MobileNetworkUtils {

    @Override
    public void switchBluetoothOn() {

    }

    @Override
    public void switchBluetoothOff() {

    }

    @Override
    public void pairBluetooth(String device) {

    }

    @Override
    public void unPairBluetooth(String device) {

    }

    @Override
    protected int getBluetoothState() {
        return 0;
    }

    @Override
    protected boolean isPairedBluetooth(String device) {
        return false;
    }

    @Override
    public boolean isWifiEnabled() {
        boolean enabled = ((AndroidDriver) this.getDriver()).getConnection().isWiFiEnabled();
        log.info("Wi-Fi enabled: " + enabled);
        return enabled;
    }

    @Override
    public void enableWifi() {
        boolean enabled = this.isWifiEnabled();
        if (!enabled) {
            ((AndroidDriver) this.getDriver()).toggleWifi();
        } else {
            log.info("Wifi is already enabled. No actions needed");
        }
    }

    @Override
    public void disableWifi() {
        boolean enabled = this.isWifiEnabled();
        if (enabled) {
            ((AndroidDriver) this.getDriver()).toggleWifi();
        } else {
            log.info("Wifi is already disabled. No actions needed");
        }
    }
}
