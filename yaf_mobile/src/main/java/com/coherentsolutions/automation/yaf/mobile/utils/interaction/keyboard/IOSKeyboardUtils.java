package com.coherentsolutions.automation.yaf.mobile.utils.interaction.keyboard;

/*-
 * #%L
 * Yaf Mobile Module
 * %%
 * Copyright (C) 2020 - 2024 Coherent Solutions Inc.
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.mobile.condition.IOS;
import io.appium.java_client.MobileBy;
import io.appium.java_client.ios.IOSDriver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@IOS
public class IOSKeyboardUtils extends KeyboardMobileUtils {

    public IOSDriver getDriver() {
        return (IOSDriver) super.getDriver();
    }

    @Override
    public void hideKeyboard() {
        if (isKeyboardShown()) {
            log.info("Hiding IOS keyboard");
            try {
                getDriver().hideKeyboard();
            } catch (Exception e) {
                if (!e.getMessage().contains("Soft keyboard not present, cannot hide keyboard")) {
                    log.error("Exception appears during hideKeyboard: " + e);
                }
            }
        }
    }

    @Override
    public void tapPhoneBackButton() {
        throw new UnsupportedOperationException("Tapping physical phone button is not supported for iOS.");
    }

    @Override
    public boolean isKeyboardShown() {
        IOSDriver<?> driver = getDriver();
        boolean present = driver.isKeyboardShown();
        log.info("Is keyboard shown: " + present);
        return present;
    }

    public void tapKeyboardButton(String button) {
        log.info("Tapping IOS keyboard button: " + button);
        getDriver()
                .findElement(MobileBy.iOSClassChain(
                        String.format("**/XCUIElementTypeKeyboard/**/XCUIElementTypeKey[`name == '%s'`]", button)))
                .click();
    }
}
