package com.coherentsolutions.automation.yaf.mobile.utils.interaction.orientation;

/*-
 * #%L
 * Yaf Mobile Module
 * %%
 * Copyright (C) 2020 - 2024 Coherent Solutions Inc.
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.mobile.utils.MobileUtils;
import com.coherentsolutions.automation.yaf.mobile.utils.interaction.swipe.SwipeMobileUtils;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebDriverException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import static java.lang.String.format;
import static org.openqa.selenium.ScreenOrientation.LANDSCAPE;
import static org.openqa.selenium.ScreenOrientation.PORTRAIT;

@Component
@Slf4j
public class OrientationMobileUtils extends MobileUtils {

    @Autowired
    SwipeMobileUtils swipeMobileUtils;

    public boolean isInPortraitMode() {
        return getDriver().getOrientation().equals(PORTRAIT);
    }

    public boolean isInLandscapeMode() {
        return getDriver().getOrientation().equals(LANDSCAPE);
    }

    public void setOrientation(ScreenOrientation orientation) {
        if (!getActualOrientation().equals(orientation)) {
            rotateToOrientationAndAssert(orientation);
        }
    }

    public void rotateToLandscape() {
        rotate(LANDSCAPE);
    }

    public void assertLandscapeOrientation() {
        assertOrientation(LANDSCAPE);
    }

    public void rotateToLandscapeAndAssert() {
        rotateToOrientationAndAssert(LANDSCAPE);
    }

    public void rotateToLandscapeAndAssertAppRefusedToRotate() {
        rotateToOrientationAndAssertAppRefusedToRotate(LANDSCAPE, PORTRAIT);
    }

    public void rotateToPortrait() {
        rotate(PORTRAIT);
    }

    public void assertPortraitOrientation() {
        assertOrientation(PORTRAIT);
    }

    public void rotateToPortraitAndAssert() {
        rotateToOrientationAndAssert(PORTRAIT);
    }

    public void rotateToPortraitAndAssertAppRefusedToRotate() {
        rotateToOrientationAndAssertAppRefusedToRotate(PORTRAIT, LANDSCAPE);
    }

    private void rotateToOrientationAndAssert(ScreenOrientation orientation) {
        rotate(orientation);
        assertOrientation(orientation);
    }

    private void rotateToOrientationAndAssertAppRefusedToRotate(ScreenOrientation expectedOrientation,
                                                                ScreenOrientation actualOrientation) {
        try {
            rotate(expectedOrientation);
        } catch (WebDriverException e) {
            // todo???
        }
        assertOrientation(actualOrientation);
    }

    private void rotate(ScreenOrientation orientation) {
        log.info("Rotating screen to [{}] orientation", orientation);
        getDriver().rotate(orientation);
        swipeMobileUtils.singleSwipeDown();
    }

    private void assertOrientation(ScreenOrientation orientation) {
        log.info("Asserting screen is in [{}] orientation", orientation);
        Assert.isTrue(getActualOrientation().equals(orientation),
                format("Screen's orientation is not a %s one", orientation));
    }

    public ScreenOrientation getActualOrientation() {
        ScreenOrientation so = getDriver().getOrientation();
        log.info("Getting actual screen's orientation: {}", so);
        return so;
    }

}
