package com.coherentsolutions.automation.yaf.mobile.config;

/*-
 * #%L
 * Yaf Mobile Module
 * %%
 * Copyright (C) 2020 - 2024 Coherent Solutions Inc.
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.ExecutionService;
import com.coherentsolutions.automation.yaf.core.config.env.resolver.ConfigurationResolver;
import com.coherentsolutions.automation.yaf.core.consts.Consts;
import com.coherentsolutions.automation.yaf.core.context.test.TestExecutionContext;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Service;

@Data
@Accessors(chain = true)
@Service
public class MobileApplicationConfigurationResolver extends ConfigurationResolver {
    @Override
    public String getType() {
        return "mobileapp";
    }// TODO validate?

    @Override
    public void applyConfiguration(String value, TestExecutionContext testExecutionContext) {
        System.out.println("Setting mobileApplication " + value);
        ExecutionService.getInstance().getConfigDataValue(getType(), value).ifPresent(app -> {
            testExecutionContext.addPram(Consts.CTX_APP, app);
        });
    }
}
