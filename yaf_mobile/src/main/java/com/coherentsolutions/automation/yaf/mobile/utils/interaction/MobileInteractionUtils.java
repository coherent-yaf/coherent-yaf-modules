package com.coherentsolutions.automation.yaf.mobile.utils.interaction;

/*-
 * #%L
 * Yaf Mobile Module
 * %%
 * Copyright (C) 2020 - 2024 Coherent Solutions Inc.
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.mobile.utils.MobileUtils;
import com.coherentsolutions.automation.yaf.mobile.utils.interaction.keyboard.KeyboardMobileUtils;
import com.coherentsolutions.automation.yaf.mobile.utils.interaction.orientation.OrientationMobileUtils;
import com.coherentsolutions.automation.yaf.mobile.utils.interaction.scroll.ScrollMobileUtils;
import com.coherentsolutions.automation.yaf.mobile.utils.interaction.swipe.SwipeMobileUtils;
import com.google.common.collect.ImmutableMap;
import io.appium.java_client.appmanagement.ApplicationState;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriverException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//TODO validate and refactor

@Component
@Slf4j
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class MobileInteractionUtils extends MobileUtils {

    @Autowired
    KeyboardMobileUtils keyboard;

    @Autowired
    OrientationMobileUtils orientation;

    @Autowired
    ScrollMobileUtils scroll;

    @Autowired
    SwipeMobileUtils swipe;

    // TODO think about this methods

    public void closeApp() {
        log.info("Closing app");
        getDriver().closeApp();
    }

    public void launchApp() {
        log.info("Launching app");
        getDriver().launchApp();
    }

    public void installApp(String apkPath) {
        log.info("Will install application with apk-file from " + apkPath);
        getDriver().installApp(apkPath);
    }

    public boolean isApplicationInstalled(String packageName) {
        boolean installed = getDriver().isAppInstalled(packageName);
        log.info(String.format("Application by package name (%s) installed: ", packageName) + installed);
        return installed;
    }

    public void openDeeplink(String link, String packageName) {
        ImmutableMap preparedCommand = ImmutableMap.of("url", link, "package", packageName);

        try {
            getDriver().executeScript("mobile:deepLink", new Object[]{preparedCommand});
        } catch (WebDriverException var5) {
            log.error("org.openqa.selenium.WebDriverException is caught and ignored.");
        }

    }

    public boolean uninstallApp(String packageName) {
        boolean isUninstalled = false;
        if (isApplicationInstalled(packageName)) {
            isUninstalled = getDriver().removeApp(packageName);
        } else {
            log.info("Application is not removed due to it was not installed before");
        }
        if (isUninstalled) {
            log.info("Application has been successfully uninstalled");
            return true;
        } else {
            throw new RuntimeException("Application was not uninstalled");
        }
    }

    public void terminateApp() {
        String bundleId = getBundleId();
        getDriver().terminateApp(bundleId);
    }

    public abstract String getBundleId();

    public boolean isAppRunning() {
        String bundleId = getBundleId();
        ApplicationState actualApplicationState = getDriver().queryAppState(bundleId);
        return ApplicationState.RUNNING_IN_FOREGROUND.equals(actualApplicationState);
    }

}
