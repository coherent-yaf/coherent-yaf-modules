package com.coherentsolutions.automation.yaf.mobile.utils.interaction.swipe;

/*-
 * #%L
 * Yaf Mobile Module
 * %%
 * Copyright (C) 2020 - 2024 Coherent Solutions Inc.
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.mobile.utils.MobileUtils;
import com.coherentsolutions.automation.yaf.mobile.utils.interaction.scroll.ScrollMobileUtils;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
@Slf4j
public class SwipeMobileUtils extends MobileUtils {

    @Autowired
    ScrollMobileUtils scrollMobileUtils;

    public void swipeToScreensTop() {
        int numberOfIterations = 5;
        for (int i = 0; i < numberOfIterations; i++) {
            singleSwipeDown();
        }
    }

    public void singleSwipe(MobileElement scrollableElement, double coeffFrom, double coeffTo) {
        singleSwipeFromPoint1ToPoint2(getSwipePoint(scrollableElement, coeffFrom),
                getSwipePoint(scrollableElement, coeffTo));
    }

    public void singleSwipeDown() {
        singleSwipeFromPoint1ToPoint2(getTopSwipePoint(), getBottomSwipePoint());
    }

    private void singleSwipeFromPoint1ToPoint2(PointOption point1, PointOption point2) {
        if (point1 != null && point2 != null) {
            new TouchAction(getDriver()).longPress(point1).moveTo(point2).release().perform();
        }
    }

    private PointOption getTopSwipePoint() {
        return getSwipePoint(0.2);
    }

    private PointOption getBottomSwipePoint() {
        return getSwipePoint(0.8);
    }

    private PointOption getSwipePoint(MobileElement element, double coeff) {
        Pair<Point, Dimension> params = scrollMobileUtils.getScrollableAreaLocationAndSize(element);
        return PointOption.point(getX(params), getY(params, coeff));
    }

    private PointOption getSwipePoint(double coeff) {
        Pair<Point, Dimension> params;
        if ((params = scrollMobileUtils.getScrollableAreaLocationAndSize()) != null) {
            return PointOption.point(getX(params), getY(params, coeff));
        } else {
            return null;
        }
    }

    private int getX(Pair<Point, Dimension> params) {
        return params.getRight().getWidth() / 2 + params.getLeft().getX();
    }

    private int getY(Pair<Point, Dimension> params, double coeff) {
        return params.getLeft().getY() + (int) (params.getRight().getHeight() * coeff);
    }

    public void singleSwipeUp() {
        singleSwipeFromPoint1ToPoint2(getBottomSwipePoint(), getTopSwipePoint());
    }

    public boolean swipeToElement(MobileElement element) {
        return this.swipeToElement(element, null, SwipeMobileUtils.Direction.UP, 50, 1000);
    }

    public boolean swipeToElement(MobileElement element, int count) {
        return this.swipeToElement(element, null, SwipeMobileUtils.Direction.UP, count, 1000);
    }

    public boolean swipeToElement(MobileElement element, SwipeMobileUtils.Direction direction) {
        return this.swipeToElement(element, null, direction, 50, 1000);
    }

    public boolean swipeToElement(MobileElement element, int count, int duration) {
        return this.swipeToElement(element, null, SwipeMobileUtils.Direction.UP, count, duration);
    }

    public boolean swipeToElement(MobileElement element, By elementBy, SwipeMobileUtils.Direction direction, int count,
                                  int duration) {
        return this.swipeToElement(element, elementBy, null, direction, count, duration);
    }

    public boolean swipeToElement(MobileElement element, By elementBy, MobileElement container, int count) {
        return this.swipeToElement(element, elementBy, container, SwipeMobileUtils.Direction.UP, count, 1000);
    }

    public boolean swipeToElement(MobileElement element, By elementBy, MobileElement container) {
        return this.swipeToElement(element, elementBy, container, SwipeMobileUtils.Direction.UP, 50, 1000);
    }

    public boolean swipeToElement(MobileElement element, By elementBy, MobileElement container,
                                  SwipeMobileUtils.Direction direction) {
        return this.swipeToElement(element, elementBy, container, direction, 50, 1000);
    }

    public boolean swipeToElement(MobileElement element, By elementBy, MobileElement container,
                                  SwipeMobileUtils.Direction direction, int count) {
        return this.swipeToElement(element, elementBy, container, direction, count, 1000);
    }

    public boolean swipeToElement(MobileElement element, By elementBy, MobileElement container,
                                  SwipeMobileUtils.Direction direction, int count, int duration) {
        boolean isVisible = isElementVisible(element, elementBy, 1L);
        if (isVisible) {
            log.info("element already present before swipe: " + element.getTagName());
            return true;
        } else {
            log.info("swiping to element: " + element.getTagName());
            SwipeMobileUtils.Direction oppositeDirection;
            boolean bothDirections = false;
            switch (direction) {
                case UP:
                    oppositeDirection = SwipeMobileUtils.Direction.DOWN;
                    break;
                case DOWN:
                    oppositeDirection = SwipeMobileUtils.Direction.UP;
                    break;
                case LEFT:
                    oppositeDirection = SwipeMobileUtils.Direction.RIGHT;
                    break;
                case RIGHT:
                    oppositeDirection = SwipeMobileUtils.Direction.LEFT;
                    break;
                case HORIZONTAL:
                    direction = SwipeMobileUtils.Direction.LEFT;
                    oppositeDirection = SwipeMobileUtils.Direction.RIGHT;
                    bothDirections = true;
                    break;
                case HORIZONTAL_RIGHT_FIRST:
                    direction = SwipeMobileUtils.Direction.RIGHT;
                    oppositeDirection = SwipeMobileUtils.Direction.LEFT;
                    bothDirections = true;
                    break;
                case VERTICAL:
                    direction = SwipeMobileUtils.Direction.UP;
                    oppositeDirection = SwipeMobileUtils.Direction.DOWN;
                    bothDirections = true;
                    break;
                case VERTICAL_DOWN_FIRST:
                    direction = SwipeMobileUtils.Direction.DOWN;
                    oppositeDirection = SwipeMobileUtils.Direction.UP;
                    bothDirections = true;
                    break;
                default:
                    throw new RuntimeException("Unsupported direction for swipeInContainerTillElement: " + direction);
            }

            int currentCount;
            for (currentCount = count; !isVisible
                    && currentCount-- > 0; isVisible = isElementVisible(element, elementBy, 1L)) {
                log.debug("Element not present! Swipe " + direction + " will be executed to element: "
                        + element.getTagName());
                this.swipeInWebElement(container, direction, duration);
                log.info("Swipe was executed. Attempts remain: " + currentCount);
            }

            for (currentCount = count; bothDirections && !isVisible
                    && currentCount-- > 0; isVisible = isElementVisible(element, elementBy, 1L)) {
                log.debug("Element not present! Swipe " + oppositeDirection + " will be executed to element: "
                        + element.getTagName());
                this.swipeInWebElement(container, oppositeDirection, duration);
                log.info("Swipe was executed. Attempts remain: " + currentCount);
            }

            log.info("Result: " + isVisible);
            return isVisible;
        }
    }

    public boolean isElementVisible(MobileElement element, By elementBy, long timeout) {
        ExpectedCondition waitCondition;
        if (element != null) {
            waitCondition = ExpectedConditions
                    .or(new ExpectedCondition[]{ExpectedConditions.visibilityOfElementLocated(elementBy),
                            ExpectedConditions.visibilityOf(element)});
        } else {
            waitCondition = ExpectedConditions.visibilityOfElementLocated(elementBy);
        }

        return this.waitUntil(waitCondition, timeout);
    }

    private boolean waitUntil(ExpectedCondition<?> condition, long timeout) {
        WebDriver drv = this.getDriver();
        FluentWait wait = (new WebDriverWait(drv, timeout, 5)).ignoring(WebDriverException.class)
                .ignoring(NoSuchSessionException.class);

        boolean result;
        try {
            log.debug("waitUntil: starting...");
            log.debug("waitUntil: starting condition: " + condition.toString());
            wait.until(condition);
            result = true;
            log.debug("waitUntil: finished true...");
        } catch (NoSuchElementException var8) {
            log.debug("waitUntil: NoSuchElementException e...");
            result = false;
        } catch (TimeoutException var9) {
            log.debug("waitUntil: TimeoutException e...");
            result = false;
        } catch (WebDriverException var10) {
            log.debug("waitUntil: WebDriverException e...");
            result = false;
        } catch (Exception var11) {
            log.error("waitUntil: ", var11);
            result = false;
        }

        return result;
    }

    public void swipeToElement(int startX, int startY, int endX, int endY, int duration) {
        log.debug("Starting swipe...");
        WebDriver drv = getDriver();
        log.debug("Getting driver dimension size...");
        Dimension scrSize = drv.manage().window().getSize();
        log.debug("Finished driver dimension size...");
        if (endX >= scrSize.width) {
            log.warn("endx coordinate is bigger then device width! It will be limited!");
            endX = scrSize.width - 1;
        } else {
            endX = Math.max(1, endX);
        }

        if (endY >= scrSize.height) {
            log.warn("endy coordinate is bigger then device height! It will be limited!");
            endY = scrSize.height - 1;
        } else {
            endY = Math.max(1, endY);
        }

        log.debug("startx: " + startX + "; starty: " + startY + "; endx: " + endX + "; endy: " + endY + "; duration: "
                + duration);
        PointOption<?> startPoint = PointOption.point(startX, startY);
        PointOption<?> endPoint = PointOption.point(endX, endY);
        WaitOptions waitOptions = WaitOptions.waitOptions(Duration.ofMillis(duration));
        (new TouchAction((MobileDriver) drv)).press(startPoint).waitAction(waitOptions).moveTo(endPoint).release()
                .perform();
        log.debug("Finished swipe...");
    }

    public boolean swipeInWebElement(WebElement container, SwipeMobileUtils.Direction direction, int duration) {
        return this.swipeInWebElement(container, direction, 1, duration);
    }

    public boolean swipeInWebElement(WebElement container, SwipeMobileUtils.Direction direction, int count,
                                     int duration) {
        Point elementLocation;
        Dimension elementDimensions;
        if (container == null) {
            WebDriver driver = getDriver();
            elementLocation = new Point(0, 0);
            elementDimensions = driver.manage().window().getSize();
        } else {
            elementLocation = container.getLocation();
            elementDimensions = container.getSize();
        }

        double minCoefficient = 0.3D;
        double maxCoefficient = 0.6D;

        int startx;
        int starty;
        int endx;
        int endy;
        switch (direction) {
            case UP:
                startx = endx = elementLocation.getX() + Math.round((float) (elementDimensions.getWidth() / 2));
                starty = (int) ((long) elementLocation.getY()
                        + Math.round(maxCoefficient * (double) elementDimensions.getHeight()));
                endy = (int) ((long) elementLocation.getY()
                        + Math.round(minCoefficient * (double) elementDimensions.getHeight()));
                break;
            case DOWN:
                startx = endx = elementLocation.getX() + Math.round((float) (elementDimensions.getWidth() / 2));
                starty = (int) ((long) elementLocation.getY()
                        + Math.round(minCoefficient * (double) elementDimensions.getHeight()));
                endy = (int) ((long) elementLocation.getY()
                        + Math.round(maxCoefficient * (double) elementDimensions.getHeight()));
                break;
            case LEFT:
                starty = endy = elementLocation.getY() + Math.round((float) (elementDimensions.getHeight() / 2));
                startx = (int) ((long) elementLocation.getX()
                        + Math.round(maxCoefficient * (double) elementDimensions.getWidth()));
                endx = (int) ((long) elementLocation.getX()
                        + Math.round(minCoefficient * (double) elementDimensions.getWidth()));
                break;
            case RIGHT:
                starty = endy = elementLocation.getY() + Math.round((float) (elementDimensions.getHeight() / 2));
                startx = (int) ((long) elementLocation.getX()
                        + Math.round(minCoefficient * (double) elementDimensions.getWidth()));
                endx = (int) ((long) elementLocation.getX()
                        + Math.round(maxCoefficient * (double) elementDimensions.getWidth()));
                break;
            default:
                throw new RuntimeException("Unsupported direction: " + direction);
        }

        log.debug(String.format("Swipe from (X = %d; Y = %d) to (X = %d; Y = %d)", startx, starty, endx, endy));

        try {
            for (int i = 0; i < count; ++i) {
                this.swipeToElement(startx, starty, endx, endy, duration);
            }

            return true;
        } catch (Exception var17) {
            log.error(String.format("Error during Swipe from (X = %d; Y = %d) to (X = %d; Y = %d): %s", startx, starty,
                    endx, endy, var17));
            return false;
        }
    }

    public enum Direction {
        LEFT, RIGHT, UP, DOWN, VERTICAL, HORIZONTAL, VERTICAL_DOWN_FIRST, HORIZONTAL_RIGHT_FIRST;
    }

}
