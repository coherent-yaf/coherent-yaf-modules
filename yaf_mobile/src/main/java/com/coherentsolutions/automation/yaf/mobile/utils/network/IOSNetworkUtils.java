package com.coherentsolutions.automation.yaf.mobile.utils.network;

/*-
 * #%L
 * Yaf Mobile Module
 * %%
 * Copyright (C) 2020 - 2024 Coherent Solutions Inc.
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.mobile.condition.IOS;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.appmanagement.ApplicationState;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;

import static io.appium.java_client.appmanagement.ApplicationState.*;

@Component
@Slf4j
@IOS
public class IOSNetworkUtils extends MobileNetworkUtils {

    public static final String PREFERENCES_BUNDLE_ID = "com.apple.Preferences";
    public static final String SWITCH_BLUETOOTH_LOCATOR = "//XCUIElementTypeSwitch[@visible='true' and @name='Bluetooth']";

    @Override
    public void switchBluetoothOn() {
        goToBluetoothSettingsScreen();
        if (getBluetoothState() == 0) {
            clickOnBluetoothSwitch();
        }
    }

    @Override
    public void switchBluetoothOff() {
        goToBluetoothSettingsScreen();
        if (getBluetoothState() == 1) {
            clickOnBluetoothSwitch();
        }
    }

    @Override
    public void pairBluetooth(String device) {
        goToBluetoothSettingsScreen();
        if (!isPairedBluetooth(device)) {
            getDeviceListItem(device).click();
        }
    }

    @Override
    public void unPairBluetooth(String device) {
        throw new NotImplementedException("Not implemented");
    }

    @Override
    public boolean isWifiEnabled() {
        throw new NotImplementedException("Not implemented");
    }

    @Override
    public void enableWifi() {
        throw new NotImplementedException("Not implemented");
    }

    @Override
    public void disableWifi() {
        throw new NotImplementedException("Not implemented");
    }

    @Override
    protected int getBluetoothState() {
        return getBluetoothSwitch().getAttribute("value").equals("1") ? 1 : 0;
    }

    @Override
    protected boolean isPairedBluetooth(String device) {
        return getDeviceListItem(device).getText().equals("Connected");
    }

    protected void goToBluetoothSettingsScreen() throws IllegalStateException {
        AppiumDriver appiumDriver = getDriver();
        ApplicationState state = appiumDriver.queryAppState(PREFERENCES_BUNDLE_ID);

        if (state.equals(NOT_RUNNING)) {
            // just start app
            appiumDriver.activateApp(PREFERENCES_BUNDLE_ID);
            clickOnBluetoothSettingsButton();
        }
        if (state.equals(RUNNING_IN_BACKGROUND) || state.equals(RUNNING_IN_BACKGROUND_SUSPENDED)) {
            appiumDriver.activateApp(PREFERENCES_BUNDLE_ID);
        }
        // now we have preferences app active, try to detect on what page we are
        WebElement vpnElement = null;
        try {
            vpnElement = appiumDriver.findElementByAccessibilityId("VPN");
        } catch (Exception ex) {
            //
        }
        if (vpnElement == null) {
            // we are somewhere in settings, lets reset app
            appiumDriver.terminateApp(PREFERENCES_BUNDLE_ID);
            appiumDriver.activateApp(PREFERENCES_BUNDLE_ID);
        }
        // hope we are on main settings page
        clickOnBluetoothSettingsButton();
    }

    protected void clickOnBluetoothSettingsButton() throws IllegalStateException {
        try {
            AppiumDriver appiumDriver = getDriver();
            appiumDriver.findElementByAccessibilityId("Bluetooth").click();
        } catch (Exception ex) {
            throw new IllegalStateException("Unable to go to bluetooth page!");
        }
    }

    protected WebElement getBluetoothSwitch() {
        AppiumDriver appiumDriver = getDriver();
        return appiumDriver.findElementByXPath(SWITCH_BLUETOOTH_LOCATOR);
    }

    protected WebElement getDeviceListItem(String device) {
        AppiumDriver appiumDriver = getDriver();
        return appiumDriver.findElementByAccessibilityId(device);
    }

    protected void clickOnBluetoothSwitch() {
        getBluetoothSwitch().click();
    }
}
