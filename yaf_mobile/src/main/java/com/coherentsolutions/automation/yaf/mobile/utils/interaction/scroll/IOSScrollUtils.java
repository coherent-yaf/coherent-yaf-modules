package com.coherentsolutions.automation.yaf.mobile.utils.interaction.scroll;

/*-
 * #%L
 * Yaf Mobile Module
 * %%
 * Copyright (C) 2020 - 2024 Coherent Solutions Inc.
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.mobile.condition.Android;
import io.appium.java_client.MobileBy;
import io.appium.java_client.ios.IOSDriver;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
@Slf4j
@Android
public class IOSScrollUtils extends ScrollMobileUtils {

    public IOSDriver getDriver() {
        return (IOSDriver) super.getDriver();
    }

    @Override
    public void scrollDown() {
        scroll("down");
    }

    @Override
    public void scrollUp() {
        scroll("up");
    }

    protected void scroll(String direction) {
        HashMap<String, String> obj = new HashMap<>();
        obj.put("direction", direction);
        getDriver().executeScript("mobile: scroll", obj);
    }

    @Override
    public Pair<Point, Dimension> getScrollableAreaLocationAndSize() {
        return getScrollableAreaLocationAndSize(MobileBy.iOSClassChain("**/XCUIElementTypeScrollView"),
                MobileBy.iOSClassChain("**/XCUIElementTypeTable"));
    }
}
