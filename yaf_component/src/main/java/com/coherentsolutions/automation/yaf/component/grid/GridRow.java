package com.coherentsolutions.automation.yaf.component.grid;

/*-
 * #%L
 * Yaf Component Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import lombok.Data;
import lombok.experimental.Accessors;
import org.openqa.selenium.WebElement;

import java.util.List;

@Data
@Accessors(chain = true)
public class GridRow {
    List<GridCell> cells;
    WebElement element;

    public GridRow(WebElement element) {
        this.element = element;
    }

    public <T> T getFirstExtra(Class extraClass) {
        return (T) cells.get(0).getExtra(extraClass);
    }

    public <T> T getLastExtra(Class extraClass) {
        return (T) cells.get(cells.size() - 1).getExtra(extraClass);
    }

    public GridCell getFirst() {
        return cells.get(0);
    }

    public GridCell getLast() {
        return cells.get(cells.size() - 1);
    }
}
