package com.coherentsolutions.automation.yaf.component.grid;

/*-
 * #%L
 * Yaf Component Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.component.basic.YafWaitable;
import com.coherentsolutions.automation.yaf.core.utils.by.YafBy;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public interface YafGrid extends YafWaitable {

    WebElement get(YafBy selector);

    List<WebElement> getList(YafBy selector);

    default GridRow getTableHeader() {
        return buildHeaderRow();
    }

    default GridRow getRow(int index) {
        return buildRow(get(getRowSelector().addSelector("[" + index + "]")));
    }

    default GridRow getRowByText(String text) {
        return buildRow(get(getRowSelector().addSelector("//*[contains(text(),'" + text + "')]/ancestor::tr")));
    }

    default GridCell getCellByIndex(int rowIndex, int headerIndex) {
        return buildCell(get(getRowSelector().addSelector("[" + rowIndex + "]/td[" + headerIndex + "]")));
    }

    default GridCell getCellByIndexAndHeader(int rowIndex, String header) {
        return getCellByIndex(rowIndex, findHeaderIndex(header));
    }

    default int findHeaderIndex(String headerText) {
        List<GridCell> cells = getTableHeader().getCells();
        headerText = headerText.toLowerCase();
        for (int i = 0; i < cells.size(); i++) {
            String text = cells.get(i).getText().toLowerCase();
            if (text.contains(headerText)) {
                return i + 1;
            }
        }
        return -1;
    }

    default boolean isGridContainsText(String text) {
        return get(getTableSelector()).getAttribute("innerHTML").contains(text);
    }

    List<GridRow> getAllRows();

    void refresh();

    default YafBy getTableSelector() {
        return YafBy.xpath("//table");
    }

    default YafBy getHeaderSelector() {
        return getTableSelector().addSelector("/thead/tr/th");
    }

    default YafBy getRowSelector() {
        return getTableSelector().addSelector("/tbody/tr");
    }

    default YafBy getCellSelector() {
        return YafBy.xpath("*");
    }

    default List<WebElement> getHeaderElements() {
        return getList(getHeaderSelector());
    }

    default List<WebElement> getRowElements(WebElement row) {
        return row.findElements(getCellSelector());
    }

    default GridRow buildHeaderRow() {
        GridRow gridRow = new GridRow(null);
        gridRow.setCells(
                getHeaderElements().stream().map(e -> buildCell(e).setHeader(true)).collect(Collectors.toList()));
        return gridRow;
    }

    default GridRow buildRow(WebElement row) {
        GridRow gridRow = new GridRow(row);
        gridRow.setCells(getRowElements(row).stream().map(e -> buildCell(e)).collect(Collectors.toList()));
        return gridRow;
    }

    default GridCell buildCell(WebElement el) {
        return new GridCell(el).setText(el.getText());
    }

}
