package com.coherentsolutions.automation.yaf.component.basic;

/*-
 * #%L
 * Yaf Component Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.pom.Component;
import lombok.Data;
import lombok.experimental.Accessors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@Data
@Accessors(chain = true)
@Deprecated
public abstract class BasicComponent extends Component {

    protected By locator;
    protected WebElement element;

    public BasicComponent(By locator) {
        this.locator = locator;
    }

    public BasicComponent(String cssLocator) {
        this.locator = By.cssSelector(cssLocator);
    }

    public BasicComponent() {
        // TODO may be read locator from class annotation
    }

    public void click() {
        element.click();
    }

    public void isDisplayed() {
        element.isDisplayed();
    }

    public void isEnabled() {
        element.isEnabled();
    }

    public String getText() {
        return element.getText();
    }
}
