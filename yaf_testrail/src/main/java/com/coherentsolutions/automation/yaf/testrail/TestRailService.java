package com.coherentsolutions.automation.yaf.testrail;

/*-
 * #%L
 * Yaf TestRail Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.codepine.api.testrail.TestRail;
import com.codepine.api.testrail.model.Milestone;
import com.codepine.api.testrail.model.Result;
import com.codepine.api.testrail.model.ResultField;
import com.codepine.api.testrail.model.Run;
import com.coherentsolutions.automation.yaf.core.consts.Consts;
import com.coherentsolutions.automation.yaf.core.enums.TestState;
import com.coherentsolutions.automation.yaf.core.events.global.ExecutionStartEvent;
import com.coherentsolutions.automation.yaf.core.events.global.SuiteFinishEvent;
import com.coherentsolutions.automation.yaf.core.events.global.SuiteStartEvent;
import com.coherentsolutions.automation.yaf.core.events.test.TestFinishEvent;
import com.coherentsolutions.automation.yaf.core.test.YafTest;
import com.coherentsolutions.automation.yaf.core.utils.DateUtils;
import com.coherentsolutions.automation.yaf.core.utils.SafeParse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
@ConditionalOnProperty(name = Consts.FRAMEWORK_NAME + ".testrail.enabled", havingValue = "true")
public class TestRailService {

    @Autowired
    TestRailProperties properties;

    TestRail client;
    Milestone milestone;
    Milestone subMilestone;
    Run testRun;
    List<ResultField> customResultFields;

    int projectId;

    private void buildClient() {
        client = TestRail.builder(properties.getUrl(), properties.getUserName(), properties.getPassword()).build();
        customResultFields = client.resultFields().list().execute();
    }

    private Milestone findMilestone(Integer projectId, Predicate<? super Milestone> p) {
        return client.milestones().list(projectId).execute().stream().filter(p).findFirst().orElse(null);
    }

    private Milestone createMilestoneByName(String name, Integer projectId, Integer parentId) {
        Milestone m;
        if (parentId != null) {
            m = new MilestoneWithParent();
            ((MilestoneWithParent) m).setParentId(parentId);
        } else {
            m = new Milestone();
        }
        m.setProjectId(projectId).setName(name);
        return client.milestones().add(projectId, m).execute();
    }

    private Milestone getOrCreateMilestoneByName(String name, Integer projectId) {
        Milestone mst = findMilestone(projectId, m -> m.getName().equalsIgnoreCase(name));
        if (mst == null) {
            mst = createMilestoneByName(name, projectId, null);
        }
        return mst;
    }

    @EventListener
    public void executionStart(ExecutionStartEvent start) {
        projectId = properties.getProjectId();
        buildClient();
        milestone = getOrCreateMilestoneByName(properties.getMilestone(), projectId);

        String sumilestoneName = String.format("[Auto]_%s__(%s)_%s", start.getTestEnv(), start.getEnvConfigName(),
                DateUtils.getCurrentDateTimeByPattern("MMMd-HH:mm"));

        subMilestone = createMilestoneByName(sumilestoneName, projectId, milestone.getId());
        log.info("TestRail integration starts.\nMilestone {}\nSubmilestone {}", milestone.getUrl(),
                subMilestone.getUrl());
    }

    @EventListener
    public void suiteStart(SuiteStartEvent start) {
        // create testRun
        List<Integer> ids = start.getSuiteInfo().getTestMethods().stream()
                .flatMap(m -> getIds(AnnotationUtils.findAnnotation(m, YafTest.class))).filter(i -> i != null)
                .collect(Collectors.toList());
        testRun = client.runs().add(projectId, new Run().setName(start.getSuiteInfo().getSuiteId())
                .setMilestoneId(subMilestone.getId()).setProjectId(projectId).setCaseIds(ids).setIncludeAll(false))
                .execute();
        log.info("TestRail run created.\nRun {}", testRun.getUrl());
    }

    @EventListener
    public void suiteFinish(SuiteFinishEvent finish) {

    }

    private Stream<Integer> getIds(YafTest yafTest) {
        if (yafTest != null) {
            return Arrays.stream(yafTest.tmsIds()).map(i -> SafeParse.parseInt(i.replace("C", "")));
        } else {
            return null;
        }
    }

    @EventListener
    public void testFinish(TestFinishEvent event) {
        TestState state = event.getTestResult().getState();
        int testRailStateId = TestRailState.build(state).getNum();
        Stream<Integer> ids = getIds(event.getTestInfo().getYafTest());
        List<ResultField> customResultFields = client.resultFields().list().execute();
        if (ids != null) {
            List<Result> results = ids.map(i -> {
                Result result = new Result();
                result.setCaseId(i).setStatusId(testRailStateId);
                return result;
            }).collect(Collectors.toList());
            client.results().addForCases(testRun.getId(), results, customResultFields).execute();
        }
    }
}
