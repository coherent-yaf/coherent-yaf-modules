package com.coherentsolutions.automation.yaf.api.restassured;

/*-
 * #%L
 * Yaf API RestAssured Module
 * %%
 * Copyright (C) 2020 - 2021 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.api.restassured.log.RestAssuredLoggingFilter;
import com.coherentsolutions.automation.yaf.core.api.YafApiRequestException;
import com.coherentsolutions.automation.yaf.core.api.YafRequest;
import com.coherentsolutions.automation.yaf.core.api.auth.YafApiUser;
import com.coherentsolutions.automation.yaf.core.api.properties.ApiProperties;
import com.coherentsolutions.automation.yaf.core.consts.Consts;
import com.coherentsolutions.automation.yaf.core.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.core.utils.YafBeanUtils;
import io.restassured.config.RestAssuredConfig;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static io.restassured.RestAssured.given;

@Slf4j
public class RestAssuredYafRequest implements YafRequest<RequestSpecification, Response>, InitializingBean {

    protected RestAssuredAuthProvider authProvider;

    protected YafApiUser defaultUser;

    protected boolean takeUserFromConfig;

    protected Map<String, String> headers;

    // Be aware, that this properties are set after constructor invocation!
    @Autowired
    @Getter
    protected ApiProperties props;
    @Getter
    protected RequestSpecification requestSpecification;
    @Getter
    protected ResponseSpecification responseSpecification;
    protected RestAssuredConfig requestConfig;
    @Autowired
    protected RestAssuredLoggingFilter loggingFilter;
    @Autowired
    YafBeanUtils beanUtils;
    private Boolean baseUrlEndsWithSlash;

    @Builder(setterPrefix = "with")
    public RestAssuredYafRequest(RestAssuredAuthProvider authProvider, YafApiUser defaultUser,
            boolean takeUserFromConfig, Map<String, String> headers, RequestSpecification requestSpecification,
            ResponseSpecification responseSpecification, RestAssuredConfig requestConfig) {
        this.authProvider = authProvider;
        this.defaultUser = defaultUser;
        this.takeUserFromConfig = takeUserFromConfig;
        this.headers = headers;
        this.requestSpecification = requestSpecification;
        this.responseSpecification = responseSpecification;
        this.requestConfig = requestConfig;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        processRequestSpecification();
    }

    protected void processRequestSpecification() {
        if (requestSpecification == null) {
            requestSpecification = given();
        }
        if (requestConfig != null) {
            requestSpecification.config(requestConfig);
        }
        if (props != null) {
            requestSpecification.contentType(props.getContentType()).accept(props.getAcceptType());
            if (props.getBaseUrl() != null) {
                requestSpecification.baseUri(props.getBaseUrl());
                baseUrlEndsWithSlash = props.getBaseUrl().endsWith("/");
            }
            if (!props.isValidateSsl()) {
                requestSpecification.relaxedHTTPSValidation();
            }
            if (props.getHeaders() != null) {
                requestSpecification.headers(props.getHeaders());
            }
        }
        if (authProvider != null) {
            authProvider.setRequest(this);
        }
        requestSpecification.filter(loggingFilter);
    }

    public RequestSpecification getRequestSpecification() {
        return given().spec(requestSpecification);
    }

    public RequestSpecification anonymousReq() throws YafApiRequestException {
        return getRequestSpecification();
    }

    private YafApiUser getApiUser() throws YafApiRequestException {
        YafApiUser result = null;
        try {
            if (takeUserFromConfig) {
                TestExecutionContext testExecutionContext = beanUtils.tec();
                result = (YafApiUser) testExecutionContext.getParam(Consts.CTX_USER);
            } else {
                result = defaultUser;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        if (result == null) {
            throw new YafApiRequestException("Unable to get default user. Please specify any in configuration!");
        }
        return result;
    }

    public RequestSpecification req() throws YafApiRequestException {
        return req(getApiUser());
    }

    public RequestSpecification req(YafApiUser user) throws YafApiRequestException {
        if (authProvider == null) {
            throw new YafApiRequestException(
                    "Unable to prepare a request, cause auth provider is null. If you want to make anonym request - use 'anonymousReq' method instead.");
        }
        return authProvider.auth(user, getRequestSpecification());
    }

    // todo may be add general methods and query params and others

    @Override
    public Response anonymousReq(String method, String url, Object... body) throws YafApiRequestException {
        return makeRequest(anonymousReq(), method, url, body);
    }

    @Override
    public Response req(String method, String url, Object... body) throws YafApiRequestException {
        return req(getApiUser(), method, url, body);
    }

    @Override
    public Response req(YafApiUser user, String method, String url, Object... body) throws YafApiRequestException {
        return makeRequest(req(user), method, url, body);
    }

    protected Response makeRequest(RequestSpecification rs, String method, String url, Object... body) {
        if (body.length > 0) {
            rs.body(body[0]);
        }
        if (baseUrlEndsWithSlash != null) {
            if (!url.startsWith("/") && !baseUrlEndsWithSlash) {
                url = "/" + url;
            }
        }
        if (headers != null) {
            rs.headers(headers);
        }
        ValidatableResponse then = rs.request(method, url).then();
        if (responseSpecification != null) {
            return then.spec(responseSpecification).extract().response();
        }
        return then.extract().response();
    }
}
