package com.coherentsolutions.automation.yaf.appcenter;

/*-
 * #%L
 * yaf_appcenter
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import com.coherentsolutions.automation.yaf.core.config.env.domain.data.app.MobileApplication;
import com.coherentsolutions.automation.yaf.core.config.env.domain.device.Device;
import com.coherentsolutions.automation.yaf.core.config.env.domain.device.MobileDevice;
import com.coherentsolutions.automation.yaf.core.consts.Consts;
import com.coherentsolutions.automation.yaf.core.context.test.TestExecutionContext;
import com.coherentsolutions.automation.yaf.core.enums.MobileOS;
import com.coherentsolutions.automation.yaf.core.processor.start.env_setup.DeviceSetupProcessor;
import com.coherentsolutions.automation.yaf.core.utils.store.FilesStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Map;

@Service
@Slf4j
@ConditionalOnProperty(name = Consts.FRAMEWORK_NAME + ".appcenter.enabled", havingValue = "true")
public class AppcenterEnvSetUpProcessor implements DeviceSetupProcessor {

    @Autowired
    AppcenterProperties properties;

    @Autowired
    AppcenterService appcenterService;

    @Autowired
    FilesStore filesStore;

    @Override
    public boolean canHandle(Device device, TestExecutionContext testExecutionContext) {
        return properties.getSupportedDeviceTypes().contains(device.getType());
    }

    @Override
    @Cacheable(cacheNames = "appCenterDevices", key = "#device")
    public Map<String, Object> prepareDevice(Device device, TestExecutionContext testExecutionContext) {
        MobileApplication application = (MobileApplication) testExecutionContext.getParam(Consts.CTX_APP);
        MobileDevice mobileDevice = (MobileDevice) device;

        String type = mobileDevice.getMobileOs().name();// todo validate>>?
        String fileName = application.getName() + "." + (mobileDevice.getMobileOs().equals(MobileOS.IOS) ? "ipa" : "apk");
        try {
            File app = filesStore.getFile(type, fileName);
            if (app == null || !app.exists()) {
                app = appcenterService.downloadAppRelease(application.getName(), application.getVersion(), filesStore.getFilePathToStore(type, fileName));
            }
            // build capabilities or similar
            //TODO!!!
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
