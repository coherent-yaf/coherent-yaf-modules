package com.coherentsolutions.automation.yaf.appcenter;

/*-
 * #%L
 * yaf_appcenter
 * %%
 * Copyright (C) 2020 CoherentSolutions
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;

//TODO validate!
@Service
@Slf4j
public class AppcenterService {

    @Autowired
    AppcenterProperties properties;

    RequestSpecification specification;

    public AppcenterService() {
        specification = new RequestSpecBuilder().addHeader("X-API-Token", properties.getToken())
                .setBaseUri(properties.getUrl()).build();
    }

    public List<String> getAllAvailableApps() {
        return given(specification).get("tester/apps").jsonPath().getList("name");// todo validate
    }

    public List<AppReleaseResp> getAppReleases(String appName) {
        String url = "apps/" + properties.getOwner() + "/" + appName + "/releases";
        log.info(url);
        return Arrays.asList(given(specification).get(url)
                .as(AppReleaseResp[].class));
    }

    public AppReleaseResp getLatestAppReleases(String appName) {
        return getAppReleases(appName).get(0);
    }

    public List<AppReleaseResp> getAppReleases(String appName, int topCount) {
        List<AppReleaseResp> releaseResps = getAppReleases(appName);
        if (topCount > releaseResps.size()) {
            return releaseResps;
        } else {
            return releaseResps.subList(0, topCount);
        }
    }

    protected String getAppDownloadUrl(String appName, int buildId) {
        return given(specification).get("apps/" + properties.getOwner() + "/" + appName + "/releases/" + buildId).then()
                .extract().body().jsonPath().get("download_url");
    }

    public File downloadLatestAppRelease(String appName, String outputFile) throws IOException {
        AppReleaseResp release = getLatestAppReleases(appName);
        if (release != null) {
            return downloadAppRelease(appName, release.getId(), outputFile);
        } else {
            throw new IOException("Unable to find latest app file!");
        }
    }

    public File downloadAppRelease(String appName, String version, String outputFile) throws IOException {
        AppReleaseResp release = getAppReleases(appName, 20).stream()
                .filter(a -> version.equals(a.getVersion()) || version.equals(a.getVersionId())).findFirst()
                .orElse(null);
        if (release != null) {
            return downloadAppRelease(appName, release.getId(), outputFile);
        } else {
            throw new IOException("Unable to find app file for version " + version);
        }
    }

    public File downloadAppRelease(String appName, int buildId, String outputFile) throws IOException {
        String url = getAppDownloadUrl(appName, buildId);
        if (outputFile == null) {
            outputFile = File.separator + "tmp.app";
        }
        new File(outputFile).getParentFile().mkdirs();
        byte[] appBytes = given(specification).urlEncodingEnabled(false).get(url).asByteArray();
        return Files.write(Paths.get(outputFile), appBytes).toFile();
    }

}
