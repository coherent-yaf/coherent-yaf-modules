package com.coherentsolutions.automation.yaf.appcenter;

import com.coherentsolutions.automation.yaf.core.config.yaf.ModuleProperties;
import com.coherentsolutions.automation.yaf.core.consts.Consts;
import com.coherentsolutions.automation.yaf.core.enums.DeviceType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@Configuration
@ConfigurationProperties(prefix = Consts.FRAMEWORK_NAME + ".appcenter")
public class AppcenterProperties extends ModuleProperties {

    String url;
    String token;
    String owner;
    List<DeviceType> supportedDeviceTypes;

}
