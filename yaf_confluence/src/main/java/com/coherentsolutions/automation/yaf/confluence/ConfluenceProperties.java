package com.coherentsolutions.automation.yaf.confluence;

import com.coherentsolutions.automation.yaf.core.config.yaf.ModuleProperties;
import com.coherentsolutions.automation.yaf.core.consts.Consts;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@EqualsAndHashCode(callSuper = true)
@Configuration
@ConfigurationProperties(prefix = Consts.FRAMEWORK_NAME + ".confluence")
public class ConfluenceProperties extends ModuleProperties {

    String url;
    String userName;
    String password;

    String milestone;
    Integer projectId;
}
