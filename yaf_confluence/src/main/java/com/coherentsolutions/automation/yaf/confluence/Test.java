package com.coherentsolutions.automation.yaf.confluence;



import com.atlassian.confluence.rest.client.RemoteContentService;
import com.atlassian.confluence.rest.client.RemoteContentServiceImpl;
import com.atlassian.confluence.rest.client.RestClientFactory;
import com.atlassian.confluence.rest.client.authentication.AuthenticatedWebResourceProvider;
import com.github.crob1140.confluence.errors.ConfluenceRequestException;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.concurrent.Executors;


@Data
@Accessors(chain = true)
public class Test {


    public static void main(String[] args) throws ConfluenceRequestException {
        String baseUrl = "https://coherentsolutions.atlassian.net/wiki";
        String username = "artemmoroz@coherentsolutions.com";
        String password = "ghjcnjq_1";
        ListeningExecutorService executor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(3));
        AuthenticatedWebResourceProvider authenticatedWebResourceProvider = new AuthenticatedWebResourceProvider(
                RestClientFactory.newClient(), baseUrl, "");
        authenticatedWebResourceProvider.setAuthContext(username, password.toCharArray());
        RemoteContentService contentService = new RemoteContentServiceImpl(authenticatedWebResourceProvider, executor);

//        WebTarget wikiTarget = ClientBuilder.newClient().target(baseUrl);
//        AuthMethod basicAuth = new BasicAuth(username, password);
//        ConfluenceClient client = new ConfluenceClient(wikiTarget, basicAuth);
//        List<Content> existingPages = client.getContent(new GetContentRequest.Builder()
//                .setSpaceKey("CEVTR")
//                .setTitle("Glossary")
//                .setExpandedProperties(new ExpandedContentProperties.Builder().addVersion().build())
//                .setLimit(1)
//                .build());

        System.out.println(1);

    }
}
