package com.coherentsolutions.automation.yaf.confluence;

import com.coherentsolutions.automation.yaf.core.consts.Consts;
import com.coherentsolutions.automation.yaf.core.events.global.ExecutionReportReadyEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@ConditionalOnProperty(name = Consts.FRAMEWORK_NAME + ".confluence.enabled", havingValue = "true")
public class ConfluenceService {

    @Autowired
    ConfluenceProperties properties;


    @EventListener
    public void publishReport(ExecutionReportReadyEvent event) {

    }

}
