package com.coherentsolutions.automation.yaf.tm4j.cloud.query;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

@Data
@Accessors(chain = true)
public abstract class PagingReq {

    Integer maxResults = 20;
    Integer startAt = 0;

    public Map<String, String> getQueryParams() {
        Map<String, String> params = new HashMap<>();
        if (maxResults != null) {
            params.put("maxResults", String.valueOf(maxResults));
        }
        if (startAt != null) {
            params.put("startAt", String.valueOf(startAt));
        }
        appendProperParams(params);
        return params;
    }

    protected abstract void appendProperParams(Map<String, String> params);
}
