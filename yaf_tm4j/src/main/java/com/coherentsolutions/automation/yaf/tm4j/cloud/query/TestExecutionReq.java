package com.coherentsolutions.automation.yaf.tm4j.cloud.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Map;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class TestExecutionReq extends PagingReq {

    String testCycle;
    String testCase;

    @Override
    protected void appendProperParams(Map<String, String> params) {
        if (testCycle != null) {
            params.put("testCycle", testCycle);
        }
        if (testCase != null) {
            params.put("testCase", testCase);
        }
    }
}
