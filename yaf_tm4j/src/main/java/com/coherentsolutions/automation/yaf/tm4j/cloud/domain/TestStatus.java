package com.coherentsolutions.automation.yaf.tm4j.cloud.domain;

import com.fasterxml.jackson.annotation.JsonValue;

public enum TestStatus {

    PASS("Pass"), FAIL("Fail"), NOT_EXECUTE("Not Executed");

    String value;

    TestStatus(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

}
