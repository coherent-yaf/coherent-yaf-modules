package com.coherentsolutions.automation.yaf.tm4j.cloud.query;

import com.coherentsolutions.automation.yaf.tm4j.cloud.domain.FolderType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class FoldersReq extends PagingReq {

    FolderType folderType;

    @Override
    protected void appendProperParams(Map<String, String> params) {
        if (folderType != null) {
            params.put("folderType", folderType.getValue());
        }
    }
}
