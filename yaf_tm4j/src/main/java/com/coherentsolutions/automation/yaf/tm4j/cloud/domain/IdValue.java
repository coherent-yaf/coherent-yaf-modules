package com.coherentsolutions.automation.yaf.tm4j.cloud.domain;

import lombok.Data;

@Data
public class IdValue {

    Integer id;
    String self;
}
