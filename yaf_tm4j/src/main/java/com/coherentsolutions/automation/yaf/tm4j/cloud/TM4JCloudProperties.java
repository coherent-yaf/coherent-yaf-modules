package com.coherentsolutions.automation.yaf.tm4j.cloud;

import com.coherentsolutions.automation.yaf.core.config.yaf.ModuleProperties;
import com.coherentsolutions.automation.yaf.core.consts.Consts;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@EqualsAndHashCode(callSuper = true)
@Configuration
@ConfigurationProperties(prefix = Consts.FRAMEWORK_NAME + ".tm4j.cloud")
public class TM4JCloudProperties extends ModuleProperties {

    String projectKey;
    String apiKey;
    String tm4jUrl = "https://api.adaptavist.io/tm4j/v2/";
}
