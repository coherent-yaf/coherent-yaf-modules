package com.coherentsolutions.automation.yaf.tm4j.cloud.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString(callSuper = true)
public class TestExecution extends TM4JEntity {

    @JsonProperty("key")
    String name;
    IdValue project;
    IdValue testCase;
    IdValue environment;
    IdValue testExecutionStatus;
    Date actualEndDate;
    String comment;
    Boolean automated;
    IdValue testCycle;
    Map<String, Object> customFields;

}
