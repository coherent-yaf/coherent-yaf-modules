package com.coherentsolutions.automation.yaf.tm4j.cloud.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString(callSuper = true)
public class Cycle extends TM4JEntity {

    String key;
    String description;
    IdValue project;
    IdValue jiraProjectVersion;
    IdValue status;
    IdValue folder;
    Map<String, Object> customFields;

}
