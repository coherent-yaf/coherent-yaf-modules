package com.coherentsolutions.automation.yaf.tm4j.cloud.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CycleCreateDTO extends Cycle {

    @JsonProperty("jiraProjectVersion")
    int projectVersion;
    Integer folderId;
}
