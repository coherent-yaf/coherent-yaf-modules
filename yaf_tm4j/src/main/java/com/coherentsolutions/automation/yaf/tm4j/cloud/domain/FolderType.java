package com.coherentsolutions.automation.yaf.tm4j.cloud.domain;

import com.fasterxml.jackson.annotation.JsonValue;

public enum FolderType {

    TESTCASE("TEST_CASE"), TESTPLAN("TEST_PLAN"), TESTCYCLE("TEST_CYCLE");

    String value;

    FolderType(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
