package com.coherentsolutions.automation.yaf.tm4j.cloud.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString(callSuper = true)
public class TestExecutionCreateDTO extends TestExecution {

    String projectKey;
    String testCaseKey;
    String testCycleKey;
    TestStatus statusName;
    List<TestScriptResult> testScriptResults;
    String environmentName;
    Long executionTime;

    @Data
    @Accessors(chain = true)
    public static class TestScriptResult {

        TestStatus statusName;
        Date actualEndDate;
        String actualResult;
    }

}
